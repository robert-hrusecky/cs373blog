---
layout: post
title:  "Final Entry"
date:   2020-05-10 23:00:00 -0600
categories: class 
---

![headshot](/cs373blog/headshot.png)

## What did you like the least about the class?

I don't really think it was all that useful that we spent so much time on
Python syntax. Python is a really ubiquitous language, so I imagine that most
students already knew most of Python syntax.

## What did you like the most about the class?

I like that we got to work on a single large scale project for the entire
semester. It was really fun working with everyone and realizing our vision
together.

## What's the most significant thing you learned?

I think the most significant thing I learned was how to use SQL. I had never
had to work with databases before, and I'm glad that I finally got the chance
to learn what they are all about and how to use SQL to form arbitrary queries.

## How many hours a week did you spend coding/debugging/testing for this class?

I probably spend close to 10-15 hrs/week coding, debugging, testing, and doing
other project-related tasks.

## How many hours a week did you spend reading/studying for this class?

I spend maybe 1-2 hrs/week reading and studying. I don't feel that that was a
big part of this class.

## How many lines of code do you think you wrote?

In our final product, probably around 400-500 lines were generated at least in
part by me. However, I don't think this is really a good metric of how much
effort was put into the project. As the project evolved, lines were constantly
added and deleted, bugs were fixed, and multiple people worked on the same code
at the same time. I would say that it "feels like" I wrote around 1000-1500
lines.

## What required tool did you not know and now find very useful?

The required tool that I found most useful was probably sqlalchemy, although it
was sometimes a pain to get it to generate the exact SQL that you wanted. I
don't really think any required tool (that I wasn't already familiar with) will
end up being super important for work that I do on my own, as I don't generally
deal with web development.

## What's the most useful Web dev tool that your group used that was not required?

This was probably TypeScript. It allowed us to catch many errors, and made
overall development on the frontend side of things much easier.

## How did you feel about your group having to self-teach many, many technologies?

I was ok with this. I think most students are already familiar with researching
different technologies to use for the various projects that they have worked
on.

## How did you feel about the two-stage quizzes and tests?

I liked the two-staged tests; working with our group for the second part of the
test was helpful. The tests in general I felt sometimes had unclear directions,
especially on the last exam. I think this resulted in many students getting
questions wrong, enough that the questions were offered again on the second
half of the exam.

## How did you feel about the cold calling, in the end?

I think the could calling was useful in that it made the lectures more
engaging, not just because there was the constant fear of being called on, but
the interactivity made it more interesting to listen to. However, the entire
class was formatted this way, and at times it was tiresome. I think some
portions of lecture could have been more effective if they were given in a more
traditional lecture format (not having to rely on students not "knowing" the
answer to a question about a concept to introduce that concept).
